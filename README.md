## Introduction
This is the very first feature for Snakes and Ladders Kata game.

## Feature

1. Retrieve player by id
2. Allow to roll a die
3. Allow a player to move to a new position based on die value

## Stacks
- Spring boot
- Kotlin
- jupiter

## Reference
Snakes and Ladders Kata - https://web.archive.org/web/20210506135240/http:/agilekatas.co.uk/katas/SnakesAndLadders-Kata

### Run locally

*From your IDE*, run the following (right click on SnakesAndLaddersApplication -> run)

## PostMan collections
PostMan collection is located at: postman/Snakes and Ladders.postman_collection.json
