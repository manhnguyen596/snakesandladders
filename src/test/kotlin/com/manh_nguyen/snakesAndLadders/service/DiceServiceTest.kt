package com.manh_nguyen.snakesAndLadders.service

import com.manh_nguyen.snakesAndLadders.service.impl.DiceServiceImpl
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class DiceServiceTest {
    private val diceService: DiceService = DiceServiceImpl()

    @Test
    fun shouldRollAndGenerateValueBetweenOneAndSix() {
        //given
        val validValues = listOf(1, 2, 3, 4, 5, 6)

        //when
        val diceValue = diceService.roll()

        //then
        assertTrue(validValues.contains(diceValue.value))
    }

}