package com.manh_nguyen.snakesAndLadders.service

import com.manh_nguyen.snakesAndLadders.model.Player
import com.manh_nguyen.snakesAndLadders.repository.PlayerRepository
import com.manh_nguyen.snakesAndLadders.service.impl.PlayerServiceImpl
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest
class PlayerServiceTest {

    @MockBean
    private lateinit var playerRepository: PlayerRepository

    private lateinit var playerService: PlayerService
    private val WIN_POINT = 100

    @BeforeEach
    fun setup() {
        playerService = PlayerServiceImpl(playerRepository, WIN_POINT)
    }

    @Test
    fun shouldMovePlayerToCorrectPosition() {
        //given
        val diceValue = 1
        val playerId = 1
        val currentPosition = 10
        val player = Player(playerId, currentPosition)

        //when
        `when`(playerRepository.getById(playerId)).thenReturn(player)
        playerService.move(playerId, diceValue)

        //then
        assertEquals(player.position, diceValue + currentPosition)
    }

    @Test
    fun shouldKeepThePlayerInTheSamePositionIfNewPositionIsGreaterThanWinPoint() {
        //given
        val diceValue = 3
        val playerId = 1
        val currentPosition = 98
        val player = Player(playerId, currentPosition)

        //when
        `when`(playerRepository.getById(playerId)).thenReturn(player)
        playerService.move(playerId, diceValue)

        //then
        assertEquals(player.id, playerId)
        assertEquals(player.isWon, false)
        assertEquals(player.position, currentPosition)
    }

    @Test
    fun shouldMarkPlayerAsWonIfNewPositionIsEqualsWinPoint() {
        //given
        val diceValue = 3
        val playerId = 1
        val currentPosition = WIN_POINT - diceValue
        val player = Player(playerId, currentPosition)

        //when
        `when`(playerRepository.getById(playerId)).thenReturn(player)
        playerService.move(playerId, diceValue)

        //then
        assertEquals(player.id, playerId)
        assertEquals(player.isWon, true)
        assertEquals(player.position, WIN_POINT)
    }

    @Test
    fun shouldReturnPlayerById() {
        //given
        val playerId = 1
        val position = 10
        val player = Player(playerId, position)

        //when
        `when`(playerRepository.getById(playerId)).thenReturn(player)
        val result = playerService.getById(playerId)

        //then
        assertEquals(result.id, playerId)
        assertEquals(result.isWon, false)
        assertEquals(result.position, position)
    }
}