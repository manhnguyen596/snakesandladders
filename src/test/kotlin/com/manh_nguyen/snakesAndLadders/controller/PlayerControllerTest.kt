package com.manh_nguyen.snakesAndLadders.controller

import com.manh_nguyen.snakesAndLadders.model.Player
import com.manh_nguyen.snakesAndLadders.service.PlayerService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.doNothing
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ExtendWith(SpringExtension::class)
@WebMvcTest(PlayerController::class)
class PlayerControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var playerService: PlayerService

    @Test
    fun shouldSuccessfullyMovePlayerToCorrectPosition() {
        //when
        doNothing().`when`(playerService).move(anyInt(), anyInt())

        //then
        mockMvc.perform(
            put("/players/1/move")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"diceValue\": 2}")
                .accept(MediaType.APPLICATION_JSON_VALUE)
        )
            .andExpect(status().is2xxSuccessful)
    }

    @Test
    fun shouldReturnPlayerById() {
        //when
        `when`(playerService.getById(anyInt())).thenReturn(Player(1, 10, false))

        //then
        mockMvc.perform(get("/players/1"))
            .andExpect(status().is2xxSuccessful)
            .andExpect(jsonPath("$.id").value(1))
            .andExpect(jsonPath("$.position").value(10))
            .andExpect(jsonPath("$.isWon").value(false))
    }
}