package com.manh_nguyen.snakesAndLadders.controller

import com.manh_nguyen.snakesAndLadders.model.Dice
import com.manh_nguyen.snakesAndLadders.service.DiceService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@ExtendWith(SpringExtension::class)
@WebMvcTest(DiceController::class)
class DiceControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var diceService: DiceService

    @Test
    fun shouldRollTheDiceAndReturnCorrectValue() {
        //given

        //when
        `when`(diceService.roll()).thenReturn(Dice(2))

        //then
        mockMvc.perform(post("/dice/roll"))
            .andExpect(status().is2xxSuccessful)
            .andExpect(jsonPath("$.value").value(2))
    }
}