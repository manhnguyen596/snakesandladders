package com.manh_nguyen.snakesAndLadders.repository

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class PlayerRepositoryTest {

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Test
    fun shouldReturnPlayerById() {
        //given
        val playerId = 1

        //when
        val player = playerRepository.getById(playerId)

        //then
        assertEquals(player.id, playerId)
        assertEquals(player.isWon, false)
        assertEquals(player.position, 1)
    }

    @Test
    fun shouldThrowExceptionIfPlayerDoesNotExist() {
        //given
        val playerId = 111

        //then
        assertThrows (NoSuchElementException::class.java) {
            playerRepository.getById(playerId)
        }
    }
}