package com.manh_nguyen.snakesAndLadders.controller

import com.manh_nguyen.snakesAndLadders.model.Player
import com.manh_nguyen.snakesAndLadders.request.MovePlayerRequest
import com.manh_nguyen.snakesAndLadders.service.PlayerService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class PlayerController(private val playerService: PlayerService) {

    @PutMapping("/players/{playerId}/move")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun move(@PathVariable playerId: Int, @RequestBody request: MovePlayerRequest) {
        playerService.move(playerId, request.diceValue)
    }

    @GetMapping("/players/{id}")
    fun getById(@PathVariable id: Int): Player {
        return playerService.getById(id)
    }
}