package com.manh_nguyen.snakesAndLadders.controller

import com.manh_nguyen.snakesAndLadders.model.Dice
import com.manh_nguyen.snakesAndLadders.service.DiceService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class DiceController(private val diceService: DiceService) {

    @PostMapping("/dice/roll")
    fun roll(): Dice {
        return diceService.roll()
    }
}