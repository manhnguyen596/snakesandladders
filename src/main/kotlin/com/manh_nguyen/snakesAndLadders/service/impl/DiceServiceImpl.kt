package com.manh_nguyen.snakesAndLadders.service.impl

import com.manh_nguyen.snakesAndLadders.model.Dice
import com.manh_nguyen.snakesAndLadders.service.DiceService
import org.springframework.stereotype.Service
import java.util.Random

@Service
class DiceServiceImpl : DiceService {

    override fun roll(): Dice {
        val diceValue = Random().nextInt(6) + 1

        return Dice(diceValue)
    }
}