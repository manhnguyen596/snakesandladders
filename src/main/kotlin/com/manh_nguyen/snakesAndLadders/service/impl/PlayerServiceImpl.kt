package com.manh_nguyen.snakesAndLadders.service.impl

import com.manh_nguyen.snakesAndLadders.model.Player
import com.manh_nguyen.snakesAndLadders.repository.PlayerRepository
import com.manh_nguyen.snakesAndLadders.service.PlayerService
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class PlayerServiceImpl(private val playerRepository: PlayerRepository,
                        @Value("\${snakeAndLadders.winPoint}") private val winPoint: Int): PlayerService {

    override fun move(playerId: Int, diceValue: Int) {
        val player = playerRepository.getById(playerId)
        val newPosition = calculateNewPosition(player.position, diceValue)

        if (newPosition == winPoint) {
            player.isWon = true
        }

        player.position = newPosition
    }

    override fun getById(playerId: Int): Player {
        return playerRepository.getById(playerId)
    }

    private fun calculateNewPosition(currentPosition: Int, diceValue: Int): Int {
        val newPosition = currentPosition + diceValue

        if (newPosition > winPoint) {
            return currentPosition
        }

        return newPosition
    }
}