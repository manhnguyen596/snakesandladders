package com.manh_nguyen.snakesAndLadders.service

import com.manh_nguyen.snakesAndLadders.model.Player

interface PlayerService {
    fun move(playerId: Int, diceValue: Int)

    fun getById(playerId: Int): Player
}