package com.manh_nguyen.snakesAndLadders.service

import com.manh_nguyen.snakesAndLadders.model.Dice

interface DiceService {
    fun roll(): Dice
}