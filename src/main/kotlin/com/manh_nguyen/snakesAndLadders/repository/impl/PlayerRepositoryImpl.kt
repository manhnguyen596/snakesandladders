package com.manh_nguyen.snakesAndLadders.repository.impl

import com.manh_nguyen.snakesAndLadders.model.Player
import com.manh_nguyen.snakesAndLadders.repository.PlayerRepository
import org.springframework.stereotype.Repository

@Repository
class PlayerRepositoryImpl : PlayerRepository {
    private val players: List<Player> = listOf(Player(1, 1))

    override fun getById(playerId: Int): Player {
        return players.first { it.id == playerId }
    }
}