package com.manh_nguyen.snakesAndLadders.repository

import com.manh_nguyen.snakesAndLadders.model.Player

interface PlayerRepository {
    fun getById(playerId: Int): Player
}