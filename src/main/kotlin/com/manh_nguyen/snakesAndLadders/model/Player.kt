package com.manh_nguyen.snakesAndLadders.model

data class Player (val id: Int, var position: Int, var isWon: Boolean = false)