package com.manh_nguyen.snakesAndLadders.request

data class MovePlayerRequest(val diceValue: Int)