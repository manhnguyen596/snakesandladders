package com.manh_nguyen.snakesAndLadders

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SnakesAndLaddersApplication

fun main(args: Array<String>) {
	runApplication<SnakesAndLaddersApplication>(*args)
}
